const db = firebase.firestore();

firebase.auth().onAuthStateChanged(function(user) {
    if (!(user)) {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithRedirect(provider).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
        }).catch(function(error) {
            console.error("Error code: " + error.code + "\nError message: " + error.message + "\nError email: " + error.email);
        });
    } else {
        db.collection("users").doc(user.uid).onSnapshot(function(doc) {
            if (doc.exists) {
                var rank = rankToStr(doc.data().rank)
                if (!(rank.includes(page))) {
                    window.location.href = rank[0] + ".html"
                }
            }
        })
    }
})

//Define which user has access to a page
function rankToStr(rank) {
    switch (rank) {
        case 0:
            return (["disabled"])
            break;
        case 1:
            return (["overview", "view", "add"])
            break;
    }
}
